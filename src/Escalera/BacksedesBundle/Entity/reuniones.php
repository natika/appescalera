<?php

namespace Escalera\BacksedesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Form\Form;

/**
 * reuniones
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Escalera\BacksedesBundle\Entity\reunionesRepository")
 */
class reuniones
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="idreunion", type="string", length=255)
     */
    private $idreunion;

    /**
     * @var string
     *
     * @ORM\Column(name="detalles", type="string", length=255)
     */
    private $detalles;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_apertura", type="date")
     */
    private $fechaApertura;

    /**
     * @var boolean
     *
     * @ORM\Column(name="activo", type="boolean")
     */
    private $activo;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Escalera\BacksedesBundle\Entity\sedeDirecta")
     */
    private $sedeId;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idreunion
     *
     * @param string $idreunion
     * @return reuniones
     */
    public function setIdreunion($idreunion)
    {
        $this->idreunion = $idreunion;
    
        return $this;
    }

    /**
     * Get idreunion
     *
     * @return string 
     */
    public function getIdreunion()
    {
        return $this->idreunion;
    }

    /**
     * Set detalles
     *
     * @param string $detalles
     * @return reuniones
     */
    public function setDetalles($detalles)
    {
        $this->detalles = $detalles;
    
        return $this;
    }

    /**
     * Get detalles
     *
     * @return string 
     */
    public function getDetalles()
    {
        return $this->detalles;
    }

    /**
     * Set fechaApertura
     *
     * @param \DateTime $fechaApertura
     * @return reuniones
     */
    public function setFechaApertura($fechaApertura)
    {
        $this->fechaApertura = $fechaApertura;
    
        return $this;
    }

    /**
     * Get fechaApertura
     *
     * @return \DateTime 
     */
    public function getFechaApertura()
    {
        return $this->fechaApertura;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     * @return reuniones
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;
    
        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean 
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set sedeId
     *
     * @param string $sedeId
     * @return reuniones
     */
    public function setSedeId(\Escalera\BacksedesBundle\Entity\sedeDirecta $sedeId)
    {
        $this->sedeId = $sedeId;
    
        return $this;
    }

    /**
     * Get sedeId
     *
     * @return string 
     */
    public function getSedeId()
    {
        return $this->sedeId;
    }
    public function __toString()
    {
        return $this->getIdreunion();
    }
}
