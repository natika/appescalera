<?php

namespace Escalera\BacksedesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * equipoCiento
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class equipoCiento
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Escalera\BacksedesBundle\Entity\equipoDoce")
     */
    private $idDoce;

    /**
     * @var string
     * @ORM\Id
     * @ORM\OneToOne(targetEntity="Escalera\BacksedesBundle\Entity\personasLideres")
     */
    private $idLider;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_ingreso", type="date")
     */
    private $fechaIngreso;

    /**
     * @var boolean
     *
     * @ORM\Column(name="activo", type="boolean")
     */
    private $activo;
    
    /**
     * Set idIdDoce
     *
     * @param string $idDoce
     * @return equipoCiento
     */
    public function setIdDoce(\Escalera\BacksedesBundle\Entity\equipoDoce $idDoce)
    {
        $this->idDoce = $idDoce;
    
        return $this;
    }
    
    /**
     * Get idDoce
     *
     * @return string 
     */
    public function getIdDoce()
    {
        return $this->idDoce;
    }

    /**
     * Set idLider
     *
     * @param string $idLider
     * @return equipoCiento
     */
    public function setIdLider(\Escalera\BacksedesBundle\Entity\personasLideres $idLider)
    {
        $this->idLider = $idLider;
    
        return $this;
    }

    /**
     * Get idLider
     *
     * @return string 
     */
    public function getIdLider()
    {
        return $this->idLider;
    }

    /**
     * Set fechaIngreso
     *
     * @param \DateTime $fechaIngreso
     * @return equipo_144
     */
    public function setFechaIngreso($fechaIngreso)
    {
        $this->fechaIngreso = $fechaIngreso;
    
        return $this;
    }

    /**
     * Get fechaIngreso
     *
     * @return \DateTime 
     */
    public function getFechaIngreso()
    {
        return $this->fechaIngreso;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     * @return equipo_144
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;
    
        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean 
     */
    public function getActivo()
    {
        return $this->activo;
    }
    public function __toString() 
    {
        return $this->getIdLider();
    }
}
