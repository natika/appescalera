<?php

namespace Escalera\BacksedesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * inscEvento
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class inscEvento
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="idganado", type="string", length=255)
     */
    private $idganado;

    /**
     * @var string
     *
     * @ORM\Column(name="idlider", type="string", length=255)
     */
    private $idlider;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Escalera\BacksedesBundle\Entity\evento")
     */
    private $idEvento;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechainscripcion", type="date")
     */
    private $fechainscripcion;

    /**
     * @var integer
     *
     * @ORM\Column(name="abono", type="integer")
     */
    private $abono;

    /**
     * @var boolean
     *
     * @ORM\Column(name="bloqueado", type="boolean")
     */
    private $bloqueado;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idganado
     *
     * @param string $idganado
     * @return inscEvento
     */
    public function setIdganado($idganado)
    {
        $this->idganado = $idganado;
    
        return $this;
    }

    /**
     * Get idganado
     *
     * @return string 
     */
    public function getIdganado()
    {
        return $this->idganado;
    }

    /**
     * Set idlider
     *
     * @param string $idlider
     * @return inscEvento
     */
    public function setIdlider($idlider)
    {
        $this->idlider = $idlider;
    
        return $this;
    }

    /**
     * Get idlider
     *
     * @return string 
     */
    public function getIdlider()
    {
        return $this->idlider;
    }

    /**
     * Set idEvento
     *
     * @param string $idEvento
     * @return inscEvento
     */
    public function setIdEvento(\Escalera\BacksedesBundle\Entity\evento $idEvento)
    {
        $this->idEvento = $idEvento;
    
        return $this;
    }

    /**
     * Get idEvento
     *
     * @return string 
     */
    public function getIdEvento()
    {
        return $this->idEvento;
    }

    /**
     * Set fechainscripcion
     *
     * @param \DateTime $fechainscripcion
     * @return inscEvento
     */
    public function setFechainscripcion($fechainscripcion)
    {
        $this->fechainscripcion = $fechainscripcion;
    
        return $this;
    }

    /**
     * Get fechainscripcion
     *
     * @return \DateTime 
     */
    public function getFechainscripcion()
    {
        return $this->fechainscripcion;
    }

    /**
     * Set abono
     *
     * @param integer $abono
     * @return inscEvento
     */
    public function setAbono($abono)
    {
        $this->abono = $abono;
    
        return $this;
    }

    /**
     * Get abono
     *
     * @return integer 
     */
    public function getAbono()
    {
        return $this->abono;
    }

    /**
     * Set bloqueado
     *
     * @param boolean $bloqueado
     * @return inscEvento
     */
    public function setBloqueado($bloqueado)
    {
        $this->bloqueado = $bloqueado;
    
        return $this;
    }

    /**
     * Get bloqueado
     *
     * @return boolean 
     */
    public function getBloqueado()
    {
        return $this->bloqueado;
    }
}
