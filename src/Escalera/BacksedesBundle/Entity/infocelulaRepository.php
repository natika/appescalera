<?php

namespace Escalera\BacksedesBundle\Entity;

use Doctrine\ORM\EntityRepository;

class infocelulaRepository extends EntityRepository
{
    public function findEquipoCelula($idcelula, $sede)
    {
        $em = $this->getEntityManager();
        $consulta = $em->createQuery('
            SELECT p, c FROM EscaleraBacksedesBundle:infocelula c JOIN c.idlidercelula p
            WHERE c.id = :idcelula and c.sede = :sede
        ');
        $consulta->setParameter('idcelula', $idcelula);
        $consulta->setParameter('sede', $sede);
        return $consulta->getResult();
    }
}