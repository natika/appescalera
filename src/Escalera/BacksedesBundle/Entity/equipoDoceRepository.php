<?php

namespace Escalera\BacksedesBundle\Entity;

use Doctrine\ORM\EntityRepository;

class equipoDoceRepository extends EntityRepository
{
    public function findCelulasEquipo($id_equipo,$sede)
    {
        $em = $this->getEntityManager();
        $consulta = $em->createQuery('
            SELECT c, p FROM EscaleraBacksedesBundle:infocelula c JOIN c.idlidercelula p
        WHERE p.equipo12 = :id and p.sedeId = :sede
        ORDER By c.codigocelula ASC
        ');
        $consulta->setParameter('id', $id_equipo);
        $consulta->setParameter('sede', $sede);
        return $consulta->getResult();
    }
}