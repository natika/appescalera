<?php

namespace Escalera\BacksedesBundle\Entity;

use Doctrine\ORM\EntityRepository;

class celulasinfoRepository extends EntityRepository
{
    public function findRealizadas($id, $limit=5)
    {
        $em = $this->getEntityManager();
        $consulta = $em->createQuery('
            SELECT a FROM EscaleraBacksedesBundle:celulasinfo a 
            WHERE a.idcelula = :celula
        ORDER By a.id DESC, a.fechaRealizacion ASC
        ');
        $consulta->setMaxResults($limit);
        $consulta->setParameter('celula', $id);
        return $consulta->getResult();
    }
}