<?php

namespace Escalera\BacksedesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * infocelula
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Escalera\BacksedesBundle\Entity\infocelulaRepository")
 */
class infocelula
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(message = "No puedes crearla sin líder")
     * @ORM\ManyToOne(targetEntity="Escalera\BacksedesBundle\Entity\personasLideres")
     */
    private $idlidercelula;

    /**
     * @var string
     *
     * @ORM\Column(name="codigocelula", type="string", length=255)
     */
    private $codigocelula;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaapertura", type="date")
     */
    private $fechaapertura;

    /**
     * @var string
     *
     * @ORM\Column(name="hora", type="time", length=255)
     */
    private $hora;

    /**
     * @var string
     *
     * @ORM\Column(name="novedad", type="string", length=255)
     */
    private $novedad;

    /**
     * @var string
     *
     * @ORM\Column(name="anfitrion", type="string", length=255)
     */
    private $anfitrion;

    /**
     * @var string
     *
     * @ORM\Column(name="direccion", type="string", length=255)
     */
    private $direccion;

    /**
     * @var string
     *
     * @ORM\Column(name="barrio", type="string", length=255)
     */
    private $barrio;

    /**
     * @var boolean
     *
     * @ORM\Column(name="estado", type="boolean")
     */
    private $estado;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Escalera\BacksedesBundle\Entity\sedeDirecta")
     */
    private $sede;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idlidercelula
     *
     * @param string $idlidercelula
     * @return infocelula
     */
    public function setIdlidercelula(\Escalera\BacksedesBundle\Entity\personasLideres $idlidercelula)
    {
        $this->idlidercelula = $idlidercelula;
    
        return $this;
    }

    /**
     * Get idlidercelula
     *
     * @return string 
     */
    public function getIdlidercelula()
    {
        return $this->idlidercelula;
    }

    /**
     * Set codigocelula
     *
     * @param string $codigocelula
     * @return infocelula
     */
    public function setCodigocelula($codigocelula)
    {
        $this->codigocelula = $codigocelula;
    
        return $this;
    }

    /**
     * Get codigocelula
     *
     * @return string 
     */
    public function getCodigocelula()
    {
        return $this->codigocelula;
    }

    /**
     * Set fechaapertura
     *
     * @param \DateTime $fechaapertura
     * @return infocelula
     */
    public function setFechaapertura($fechaapertura)
    {
        $this->fechaapertura = $fechaapertura;
    
        return $this;
    }

    /**
     * Get fechaapertura
     *
     * @return \DateTime 
     */
    public function getFechaapertura()
    {
        return $this->fechaapertura;
    }

    /**
     * Set hora
     *
     * @param string $hora
     * @return infocelula
     */
    public function setHora($hora)
    {
        $this->hora = $hora;
    
        return $this;
    }

    /**
     * Get hora
     *
     * @return string 
     */
    public function getHora()
    {
        return $this->hora;
    }

    /**
     * Set novedad
     *
     * @param string $novedad
     * @return infocelula
     */
    public function setNovedad($novedad)
    {
        $this->novedad = $novedad;
    
        return $this;
    }

    /**
     * Get novedad
     *
     * @return string 
     */
    public function getNovedad()
    {
        return $this->novedad;
    }

    /**
     * Set anfitrion
     *
     * @param string $anfitrion
     * @return infocelula
     */
    public function setAnfitrion($anfitrion)
    {
        $this->anfitrion = $anfitrion;
    
        return $this;
    }

    /**
     * Get anfitrion
     *
     * @return string 
     */
    public function getAnfitrion()
    {
        return $this->anfitrion;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     * @return infocelula
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;
    
        return $this;
    }

    /**
     * Get direccion
     *
     * @return string 
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set barrio
     *
     * @param string $barrio
     * @return infocelula
     */
    public function setBarrio($barrio)
    {
        $this->barrio = $barrio;
    
        return $this;
    }

    /**
     * Get barrio
     *
     * @return string 
     */
    public function getBarrio()
    {
        return $this->barrio;
    }

    /**
     * Set estado
     *
     * @param boolean $estado
     * @return infocelula
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    
        return $this;
    }

    /**
     * Get estado
     *
     * @return boolean 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set sede
     *
     * @param string $sede
     * @return infocelula
     */
    public function setSede(\Escalera\BacksedesBundle\Entity\sedeDirecta $sede)
    {
        $this->sede = $sede;
    
        return $this;
    }

    /**
     * Get sede
     *
     * @return string 
     */
    public function getSede()
    {
        return $this->sede;
    }
    public function __toString()
    {
        return $this->getCodigocelula();
    }
}
