<?php

namespace Escalera\BacksedesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * red
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class red
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="ministero_red", type="string", length=255)
     */
    private $ministeroRed;

    /**
     * @var string
     *
     * @ORM\Column(name="id_pastor1", type="string", length=255)
     */
    private $idPastor1;

    /**
     * @var string
     *
     * @ORM\Column(name="id_pastor2", type="string", length=255)
     */
    private $idPastor2;

    /**
     * @var boolean
     *
     * @ORM\Column(name="activo", type="boolean")
     */
    private $activo;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ministeroRed
     *
     * @param string $ministeroRed
     * @return red
     */
    public function setMinisteroRed($ministeroRed)
    {
        $this->ministeroRed = $ministeroRed;
    
        return $this;
    }

    /**
     * Get ministeroRed
     *
     * @return string 
     */
    public function getMinisteroRed()
    {
        return $this->ministeroRed;
    }

    /**
     * Set idPastor1
     *
     * @param string $idPastor1
     * @return red
     */
    public function setIdPastor1($idPastor1)
    {
        $this->idPastor1 = $idPastor1;
    
        return $this;
    }

    /**
     * Get idPastor1
     *
     * @return string 
     */
    public function getIdPastor1()
    {
        return $this->idPastor1;
    }

    /**
     * Set idPastor2
     *
     * @param string $idPastor2
     * @return red
     */
    public function setIdPastor2($idPastor2)
    {
        $this->idPastor2 = $idPastor2;
    
        return $this;
    }

    /**
     * Get idPastor2
     *
     * @return string 
     */
    public function getIdPastor2()
    {
        return $this->idPastor2;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     * @return red
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;
    
        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean 
     */
    public function getActivo()
    {
        return $this->activo;
    }
    public function __toString()
    {
        return $this->getMinisteroRed();
    }
}
