<?php

namespace Escalera\BacksedesBundle\Entity;
use Doctrine\ORM\EntityRepository;

class reunionesRepository extends EntityRepository
{
    public function findTodasLasReuniones($sede)
    {
        $em = $this->getEntityManager();
        $consulta = $em->createQuery('
           SELECT *
           FROM BacksedesBundle:reuniones
           WHERE
           sede =  :id
           ORDER BY fecha_apertura ASC
        ');
        $consulta ->setParameters('id', $sede);
        
        return $consulta->getResult();
    }
}