<?php

namespace Escalera\BacksedesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * celulasinfo
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Escalera\BacksedesBundle\Entity\celulasinfoRepository")
 */
class celulasinfo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Escalera\BacksedesBundle\Entity\infocelula")
     */
    private $idcelula;

    /**
     * @var \DateTime
     * @Assert\NotBlank(message = "Fecha de realización!!")
     * @ORM\Column(name="fecha_realizacion", type="date")
     */
    private $fechaRealizacion;

    /**
     * @var integer
     * @Assert\NotBlank(message = "Cuantos asistieron??")
     * @ORM\Column(name="asistentes", type="integer")
     */
    private $asistentes;

    /**
     * @var integer
     * @Assert\NotBlank(message = "Cuanto fue la ofrenda??")
     * @ORM\Column(name="ofrenda", type="integer")
     */
    private $ofrenda;

    /**
     * @var string
     * @Assert\NotBlank(message = "Recuerda el tema de la célula")
     * @ORM\Column(name="tema", type="string", length=255)
     */
    private $tema;

    /**
     * @var string
     * @Assert\NotBlank(message = "Uppsss")
     * @ORM\Column(name="novedad", type="string", length=255)
     */
    private $novedad;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idcelula
     *
     * @param string $idcelula
     * @return celulasinfo
     */
    public function setIdcelula(\Escalera\BacksedesBundle\Entity\infocelula $idcelula)
    {
        $this->idcelula = $idcelula;
    
        return $this;
    }

    /**
     * Get idcelula
     *
     * @return string 
     */
    public function getIdcelula()
    {
        return $this->idcelula;
    }

    /**
     * Set fechaRealizacion
     *
     * @param \DateTime $fechaRealizacion
     * @return celulasinfo
     */
    public function setFechaRealizacion($fechaRealizacion)
    {
        $this->fechaRealizacion = $fechaRealizacion;
    
        return $this;
    }

    /**
     * Get fechaRealizacion
     *
     * @return \DateTime 
     */
    public function getFechaRealizacion()
    {
        return $this->fechaRealizacion;
    }

    /**
     * Set asistentes
     *
     * @param integer $asistentes
     * @return celulasinfo
     */
    public function setAsistentes($asistentes)
    {
        $this->asistentes = $asistentes;
    
        return $this;
    }

    /**
     * Get asistentes
     *
     * @return integer 
     */
    public function getAsistentes()
    {
        return $this->asistentes;
    }

    /**
     * Set ofrenda
     *
     * @param integer $ofrenda
     * @return celulasinfo
     */
    public function setOfrenda($ofrenda)
    {
        $this->ofrenda = $ofrenda;
    
        return $this;
    }

    /**
     * Get ofrenda
     *
     * @return integer 
     */
    public function getOfrenda()
    {
        return $this->ofrenda;
    }

    /**
     * Set tema
     *
     * @param string $tema
     * @return celulasinfo
     */
    public function setTema($tema)
    {
        $this->tema = $tema;
    
        return $this;
    }

    /**
     * Get tema
     *
     * @return string 
     */
    public function getTema()
    {
        return $this->tema;
    }

    /**
     * Set novedad
     *
     * @param string $novedad
     * @return celulasinfo
     */
    public function setNovedad($novedad)
    {
        $this->novedad = $novedad;
    
        return $this;
    }

    /**
     * Get novedad
     *
     * @return string 
     */
    public function getNovedad()
    {
        return $this->novedad;
    }
}
