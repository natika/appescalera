<?php

namespace Escalera\BacksedesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ganados
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class ganados
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Escalera\BacksedesBundle\Entity\equipoDoce")
     */
    private $lider12;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Escalera\BacksedesBundle\Entity\personasLideres")
     */
    private $invito;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Escalera\BacksedesBundle\Entity\personasLideres")
     */
    private $liderasignado;

    /**
     * @var string
     *
     * @ORM\Column(name="nombres", type="string", length=255)
     */
    private $nombres;

    /**
     * @var string
     *
     * @ORM\Column(name="apellidos", type="string", length=255)
     */
    private $apellidos;

    /**
     * @var integer
     *
     * @ORM\Column(name="edad", type="integer")
     */
    private $edad;

    /**
     * @var string
     *
     * @ORM\Column(name="telefono", type="string", length=255)
     */
    private $telefono;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="direccion", type="string", length=255)
     */
    private $direccion;

    /**
     * @var string
     *
     * @ORM\Column(name="barrio", type="string", length=255)
     */
    private $barrio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_invita", type="date")
     */
    private $fechaInvita;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Escalera\BacksedesBundle\Entity\reuniones")
     */
    private $reunion;

    /**
     * @var integer
     *
     * @ORM\Column(name="tiempo_asistencia", type="integer")
     */
    private $tiempoAsistencia;

    /**
     * @var string
     *
     * @ORM\Column(name="peticiones", type="string", length=255)
     */
    private $peticiones;

    /**
     * @var string
     *
     * @ORM\Column(name="genero", type="string", length=255)
     */
    private $genero;

    /**
     * @var string
     *
     * @ORM\Column(name="seguimiento", type="string", length=255)
     */
    private $seguimiento;

    /**
     * @var boolean
     *
     * @ORM\Column(name="fonovisita", type="boolean")
     */
    private $fonovisita;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_fonovista", type="datetime")
     */
    private $fechaFonovista;

    /**
     * @var boolean
     *
     * @ORM\Column(name="visita", type="boolean")
     */
    private $visita;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_visita", type="datetime")
     */
    private $fechaVisita;

    /**
     * @var boolean
     *
     * @ORM\Column(name="asistecelula", type="boolean")
     */
    private $asistecelula;

    /**
     * @var boolean
     *
     * @ORM\Column(name="asc_lider", type="boolean")
     */
    private $ascLider;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lider12
     *
     * @param string $lider12
     * @return ganados
     */
    public function setLider12(\Escalera\BacksedesBundle\Entity\equipoDoce $lider12)
    {
        $this->lider12 = $lider12;
    
        return $this;
    }

    /**
     * Get lider12
     *
     * @return string 
     */
    public function getLider12()
    {
        return $this->lider12;
    }

    /**
     * Set invito
     *
     * @param string $invito
     * @return ganados
     */
    public function setInvito(\Escalera\BacksedesBundle\Entity\personasLideres $invito)
    {
        $this->invito = $invito;
    
        return $this;
    }

    /**
     * Get invito
     *
     * @return string 
     */
    public function getInvito()
    {
        return $this->invito;
    }

    /**
     * Set liderasignado
     *
     * @param string $liderasignado
     * @return ganados
     */
    public function setLiderasignado(\Escalera\BacksedesBundle\Entity\personasLideres $liderasignado)
    {
        $this->liderasignado = $liderasignado;
    
        return $this;
    }

    /**
     * Get liderasignado
     *
     * @return string 
     */
    public function getLiderasignado()
    {
        return $this->liderasignado;
    }

    /**
     * Set nombres
     *
     * @param string $nombres
     * @return ganados
     */
    public function setNombres($nombres)
    {
        $this->nombres = $nombres;
    
        return $this;
    }

    /**
     * Get nombres
     *
     * @return string 
     */
    public function getNombres()
    {
        return $this->nombres;
    }

    /**
     * Set apellidos
     *
     * @param string $apellidos
     * @return ganados
     */
    public function setApellidos($apellidos)
    {
        $this->apellidos = $apellidos;
    
        return $this;
    }

    /**
     * Get apellidos
     *
     * @return string 
     */
    public function getApellidos()
    {
        return $this->apellidos;
    }

    /**
     * Set edad
     *
     * @param integer $edad
     * @return ganados
     */
    public function setEdad($edad)
    {
        $this->edad = $edad;
    
        return $this;
    }

    /**
     * Get edad
     *
     * @return integer 
     */
    public function getEdad()
    {
        return $this->edad;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     * @return ganados
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    
        return $this;
    }

    /**
     * Get telefono
     *
     * @return string 
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return ganados
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     * @return ganados
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;
    
        return $this;
    }

    /**
     * Get direccion
     *
     * @return string 
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set barrio
     *
     * @param string $barrio
     * @return ganados
     */
    public function setBarrio($barrio)
    {
        $this->barrio = $barrio;
    
        return $this;
    }

    /**
     * Get barrio
     *
     * @return string 
     */
    public function getBarrio()
    {
        return $this->barrio;
    }

    /**
     * Set fechaInvita
     *
     * @param \DateTime $fechaInvita
     * @return ganados
     */
    public function setFechaInvita($fechaInvita)
    {
        $this->fechaInvita = $fechaInvita;
    
        return $this;
    }

    /**
     * Get fechaInvita
     *
     * @return \DateTime 
     */
    public function getFechaInvita()
    {
        return $this->fechaInvita;
    }

    /**
     * Set reunion
     *
     * @param string $reunion
     * @return ganados
     */
    public function setReunion(\Escalera\BacksedesBundle\Entity\reuniones $reunion)
    {
        $this->reunion = $reunion;
    
        return $this;
    }

    /**
     * Get reunion
     *
     * @return string 
     */
    public function getReunion()
    {
        return $this->reunion;
    }

    /**
     * Set tiempoAsistencia
     *
     * @param integer $tiempoAsistencia
     * @return ganados
     */
    public function setTiempoAsistencia($tiempoAsistencia)
    {
        $this->tiempoAsistencia = $tiempoAsistencia;
    
        return $this;
    }

    /**
     * Get tiempoAsistencia
     *
     * @return integer 
     */
    public function getTiempoAsistencia()
    {
        return $this->tiempoAsistencia;
    }

    /**
     * Set peticiones
     *
     * @param string $peticiones
     * @return ganados
     */
    public function setPeticiones($peticiones)
    {
        $this->peticiones = $peticiones;
    
        return $this;
    }

    /**
     * Get peticiones
     *
     * @return string 
     */
    public function getPeticiones()
    {
        return $this->peticiones;
    }

    /**
     * Set genero
     *
     * @param string $genero
     * @return ganados
     */
    public function setGenero($genero)
    {
        $this->genero = $genero;
    
        return $this;
    }

    /**
     * Get genero
     *
     * @return string 
     */
    public function getGenero()
    {
        return $this->genero;
    }

    /**
     * Set seguimiento
     *
     * @param string $seguimiento
     * @return ganados
     */
    public function setSeguimiento($seguimiento)
    {
        $this->seguimiento = $seguimiento;
    
        return $this;
    }

    /**
     * Get seguimiento
     *
     * @return string 
     */
    public function getSeguimiento()
    {
        return $this->seguimiento;
    }

    /**
     * Set fonovisita
     *
     * @param boolean $fonovisita
     * @return ganados
     */
    public function setFonovisita($fonovisita)
    {
        $this->fonovisita = $fonovisita;
    
        return $this;
    }

    /**
     * Get fonovisita
     *
     * @return boolean 
     */
    public function getFonovisita()
    {
        return $this->fonovisita;
    }

    /**
     * Set fechaFonovista
     *
     * @param \DateTime $fechaFonovista
     * @return ganados
     */
    public function setFechaFonovista($fechaFonovista)
    {
        $this->fechaFonovista = $fechaFonovista;
    
        return $this;
    }

    /**
     * Get fechaFonovista
     *
     * @return \DateTime 
     */
    public function getFechaFonovista()
    {
        return $this->fechaFonovista;
    }

    /**
     * Set visita
     *
     * @param boolean $visita
     * @return ganados
     */
    public function setVisita($visita)
    {
        $this->visita = $visita;
    
        return $this;
    }

    /**
     * Get visita
     *
     * @return boolean 
     */
    public function getVisita()
    {
        return $this->visita;
    }

    /**
     * Set fechaVisita
     *
     * @param \DateTime $fechaVisita
     * @return ganados
     */
    public function setFechaVisita($fechaVisita)
    {
        $this->fechaVisita = $fechaVisita;
    
        return $this;
    }

    /**
     * Get fechaVisita
     *
     * @return \DateTime 
     */
    public function getFechaVisita()
    {
        return $this->fechaVisita;
    }

    /**
     * Set asistecelula
     *
     * @param boolean $asistecelula
     * @return ganados
     */
    public function setAsistecelula($asistecelula)
    {
        $this->asistecelula = $asistecelula;
    
        return $this;
    }

    /**
     * Get asistecelula
     *
     * @return boolean 
     */
    public function getAsistecelula()
    {
        return $this->asistecelula;
    }

    /**
     * Set ascLider
     *
     * @param boolean $ascLider
     * @return ganados
     */
    public function setAscLider($ascLider)
    {
        $this->ascLider = $ascLider;
    
        return $this;
    }

    /**
     * Get ascLider
     *
     * @return boolean 
     */
    public function getAscLider()
    {
        return $this->ascLider;
    }
    public function __toString() 
    {
        return $this->nombres." ".$this->apellidos;
    }
}
