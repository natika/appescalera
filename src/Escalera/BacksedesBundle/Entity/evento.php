<?php

namespace Escalera\BacksedesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * evento
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class evento
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Escalera\BacksedesBundle\Entity\eventoTipo")
     */
    private $tipoEvento;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_inicio", type="date")
     */
    private $fechaInicio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_fin", type="date")
     */
    private $fechaFin;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255)
     */
    private $descripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="lugar", type="string", length=255)
     */
    private $lugar;

    /**
     * @var integer
     *
     * @ORM\Column(name="duracion", type="integer")
     */
    private $duracion;

    /**
     * @var boolean
     *
     * @ORM\Column(name="activo", type="boolean")
     */
    private $activo;

    /**
     * @var string
     *
     * @ORM\Column(name="genero", type="string", length=255)
     */
    private $genero;

    /**
     * @var integer
     *
     * @ORM\Column(name="donacion", type="integer")
     */
    private $donacion;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Escalera\BacksedesBundle\Entity\sedeDirecta")
     */
    private $sede;

    /**
     * @var boolean
     *
     * @ORM\Column(name="general", type="boolean")
     */
    private $general;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tipoEvento
     *
     * @param string $tipoEvento
     * @return evento
     */
    public function setTipoEvento(\Escalera\BacksedesBundle\Entity\eventoTipo $tipoEvento)
    {
        $this->tipoEvento = $tipoEvento;
    
        return $this;
    }

    /**
     * Get tipoEvento
     *
     * @return string 
     */
    public function getTipoEvento()
    {
        return $this->tipoEvento;
    }

    /**
     * Set fechaInicio
     *
     * @param \DateTime $fechaInicio
     * @return evento
     */
    public function setFechaInicio($fechaInicio)
    {
        $this->fechaInicio = $fechaInicio;
    
        return $this;
    }

    /**
     * Get fechaInicio
     *
     * @return \DateTime 
     */
    public function getFechaInicio()
    {
        return $this->fechaInicio;
    }

    /**
     * Set fechaFin
     *
     * @param \DateTime $fechaFin
     * @return evento
     */
    public function setFechaFin($fechaFin)
    {
        $this->fechaFin = $fechaFin;
    
        return $this;
    }

    /**
     * Get fechaFin
     *
     * @return \DateTime 
     */
    public function getFechaFin()
    {
        return $this->fechaFin;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return evento
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    
        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set lugar
     *
     * @param string $lugar
     * @return evento
     */
    public function setLugar($lugar)
    {
        $this->lugar = $lugar;
    
        return $this;
    }

    /**
     * Get lugar
     *
     * @return string 
     */
    public function getLugar()
    {
        return $this->lugar;
    }

    /**
     * Set duracion
     *
     * @param integer $duracion
     * @return evento
     */
    public function setDuracion($duracion)
    {
        $this->duracion = $duracion;
    
        return $this;
    }

    /**
     * Get duracion
     *
     * @return integer 
     */
    public function getDuracion()
    {
        return $this->duracion;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     * @return evento
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;
    
        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean 
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set genero
     *
     * @param string $genero
     * @return evento
     */
    public function setGenero($genero)
    {
        $this->genero = $genero;
    
        return $this;
    }

    /**
     * Get genero
     *
     * @return string 
     */
    public function getGenero()
    {
        return $this->genero;
    }

    /**
     * Set donacion
     *
     * @param integer $donacion
     * @return evento
     */
    public function setDonacion($donacion)
    {
        $this->donacion = $donacion;
    
        return $this;
    }

    /**
     * Get donacion
     *
     * @return integer 
     */
    public function getDonacion()
    {
        return $this->donacion;
    }

    /**
     * Set sede
     *
     * @param string $sede
     * @return evento
     */
    public function setSede(\Escalera\BacksedesBundle\Entity\sedeDirecta $sede)
    {
        $this->sede = $sede;
    
        return $this;
    }

    /**
     * Get sede
     *
     * @return string 
     */
    public function getSede()
    {
        return $this->sede;
    }

    /**
     * Set general
     *
     * @param boolean $general
     * @return evento
     */
    public function setGeneral($general)
    {
        $this->general = $general;
    
        return $this;
    }

    /**
     * Get general
     *
     * @return boolean 
     */
    public function getGeneral()
    {
        return $this->general;
    }
    public function __toString()
    {
        return $this->getDescripcion();
    }
}
