<?php

namespace Escalera\BacksedesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * equipoDoce
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Escalera\BacksedesBundle\Entity\equipoDoceRepository")
 */
class equipoDoce
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * 
     * @Assert\NotBlank(message = "Debes escoger un líder")
     * @ORM\OneToOne(targetEntity="Escalera\BacksedesBundle\Entity\personasLideres")
     */
    private $idLider;

    /**
     * @var string
     * @Assert\NotBlank(message = "Debes escoger una red")
     * @ORM\ManyToOne(targetEntity="Escalera\BacksedesBundle\Entity\red")
     */
    private $idRed;

    /**
     * @var \DateTime
     * @Assert\NotBlank(message = "Debes escoger una fecha")
     * @ORM\Column(name="fecha_ingreso", type="date")
     */
    private $fechaIngreso;

    /**
     * @var \DateTime
     * @Assert\NotBlank(message = "Hey, pilas")
     * @ORM\Column(name="fecha_salida", type="date")
     */
    private $fechaSalida;

    /**
     * @var boolean
     *
     * @ORM\Column(name="activo", type="boolean")
     */
    private $activo;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Escalera\BacksedesBundle\Entity\sedeDirecta")
     */
    private $sedeId;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idLider
     *
     * @param string $idLider
     * @return equipoDoce
     */
    public function setIdLider(\Escalera\BacksedesBundle\Entity\personasLideres $idLider)
    {
        $this->idLider = $idLider;
    
        return $this;
    }

    /**
     * Get idLider
     *
     * @return string 
     */
    public function getIdLider()
    {
        return $this->idLider;
    }

    /**
     * Set idRed
     *
     * @param string $idRed
     * @return equipoDoce
     */
    public function setIdRed(\Escalera\BacksedesBundle\Entity\red $idRed)
    {
        $this->idRed = $idRed;
    
        return $this;
    }

    /**
     * Get idRed
     *
     * @return string 
     */
    public function getIdRed()
    {
        return $this->idRed;
    }

    /**
     * Set fechaIngreso
     *
     * @param \DateTime $fechaIngreso
     * @return equipoDoce
     */
    public function setFechaIngreso($fechaIngreso)
    {
        $this->fechaIngreso = $fechaIngreso;
    
        return $this;
    }

    /**
     * Get fechaIngreso
     *
     * @return \DateTime 
     */
    public function getFechaIngreso()
    {
        return $this->fechaIngreso;
    }

    /**
     * Set fechaSalida
     *
     * @param \DateTime $fechaSalida
     * @return equipoDoce
     */
    public function setFechaSalida($fechaSalida)
    {
        $this->fechaSalida = $fechaSalida;
    
        return $this;
    }

    /**
     * Get fechaSalida
     *
     * @return \DateTime 
     */
    public function getFechaSalida()
    {
        return $this->fechaSalida;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     * @return equipoDoce
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;
    
        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean 
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set sedeId
     *
     * @param string $sedeId
     * @return equipoDoce
     */
    public function setSedeId(\Escalera\BacksedesBundle\Entity\sedeDirecta $sedeId)
    {
        $this->sedeId = $sedeId;
    
        return $this;
    }

    /**
     * Get sedeId
     *
     * @return string 
     */
    public function getSedeId()
    {
        return $this->sedeId;
    }
    public function __toString()
    {
        return $this->getId()." -- ".$this->getIdLider();
    }
}
