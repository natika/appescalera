<?php

namespace Escalera\BacksedesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * sedeDirecta
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class sedeDirecta
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="\Escalera\BacksedesBundle\Entity\sedePpal")
     */
    private $sedePpal;

    /**
     * @var string
     *
     * @ORM\Column(name="sede", type="string", length=255)
     */
    private $sede;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_apertura", type="date")
     */
    private $fechaApertura;

    /**
     * @var boolean
     *
     * @ORM\Column(name="activa", type="boolean")
     */
    private $activa;

    /**
     * @var string
     *
     * @ORM\Column(name="pastor_id1", type="string", length=255)
     */
    private $pastorId1;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sedePpal
     *
     * @param string $sedePpal
     * @return sedeDirecta
     */
    public function setSedePpal(\Escalera\BacksedesBundle\Entity\sedePpal $sedePpal)
    {
        $this->sedePpal = $sedePpal;
    
        return $this;
    }

    /**
     * Get sedePpal
     *
     * @return string 
     */
    public function getSedePpal()
    {
        return $this->sedePpal;
    }

    /**
     * Set sede
     *
     * @param string $sede
     * @return sedeDirecta
     */
    public function setSede($sede)
    {
        $this->sede = $sede;
    
        return $this;
    }

    /**
     * Get sede
     *
     * @return string 
     */
    public function getSede()
    {
        return $this->sede;
    }

    /**
     * Set fechaApertura
     *
     * @param \DateTime $fechaApertura
     * @return sedeDirecta
     */
    public function setFechaApertura($fechaApertura)
    {
        $this->fechaApertura = $fechaApertura;
    
        return $this;
    }

    /**
     * Get fechaApertura
     *
     * @return \DateTime 
     */
    public function getFechaApertura()
    {
        return $this->fechaApertura;
    }

    /**
     * Set activa
     *
     * @param boolean $activa
     * @return sedeDirecta
     */
    public function setActiva($activa)
    {
        $this->activa = $activa;
    
        return $this;
    }

    /**
     * Get activa
     *
     * @return boolean 
     */
    public function getActiva()
    {
        return $this->activa;
    }

    /**
     * Set pastorId1
     *
     * @param string $pastorId1
     * @return sedeDirecta
     */
    public function setPastorId1($pastorId1)
    {
        $this->pastorId1 = $pastorId1;
    
        return $this;
    }

    /**
     * Get pastorId1
     *
     * @return string 
     */
    public function getPastorId1()
    {
        return $this->pastorId1;
    }
    public function __toString()
    {
        return $this->getSede();
    }
}
