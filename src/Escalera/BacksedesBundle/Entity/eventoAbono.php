<?php

namespace Escalera\BacksedesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * eventoAbono
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class eventoAbono
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Escalera\BacksedesBundle\Entity\inscEvento")
     */
    private $idInsc;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_abono", type="date")
     */
    private $fechaAbono;

    /**
     * @var integer
     *
     * @ORM\Column(name="abono", type="integer")
     */
    private $abono;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idInsc
     *
     * @param string $idInsc
     * @return eventoAbono
     */
    public function setIdInsc(\Escalera\BacksedesBundle\Entity\inscEvento $idInsc)
    {
        $this->idInsc = $idInsc;
    
        return $this;
    }

    /**
     * Get idInsc
     *
     * @return string 
     */
    public function getIdInsc()
    {
        return $this->idInsc;
    }

    /**
     * Set fechaAbono
     *
     * @param \DateTime $fechaAbono
     * @return eventoAbono
     */
    public function setFechaAbono($fechaAbono)
    {
        $this->fechaAbono = $fechaAbono;
    
        return $this;
    }

    /**
     * Get fechaAbono
     *
     * @return \DateTime 
     */
    public function getFechaAbono()
    {
        return $this->fechaAbono;
    }

    /**
     * Set abono
     *
     * @param integer $abono
     * @return eventoAbono
     */
    public function setAbono($abono)
    {
        $this->abono = $abono;
    
        return $this;
    }

    /**
     * Get abono
     *
     * @return integer 
     */
    public function getAbono()
    {
        return $this->abono;
    }
}
