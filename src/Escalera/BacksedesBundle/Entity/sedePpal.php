<?php

namespace Escalera\BacksedesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * sedePpal
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class sedePpal
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="sede", type="string", length=255)
     */
    private $sede;

    /**
     * @var string
     *
     * @ORM\Column(name="pastores", type="string", length=255)
     */
    private $pastores;

    /**
     * @var string
     *
     * @ORM\Column(name="sede_ppal", type="string", length=255)
     */
    private $sedePpal;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sede
     *
     * @param string $sede
     * @return sedePpal
     */
    public function setSede($sede)
    {
        $this->sede = $sede;
    
        return $this;
    }

    /**
     * Get sede
     *
     * @return string 
     */
    public function getSede()
    {
        return $this->sede;
    }

    /**
     * Set pastores
     *
     * @param string $pastores
     * @return sedePpal
     */
    public function setPastores($pastores)
    {
        $this->pastores = $pastores;
    
        return $this;
    }

    /**
     * Get pastores
     *
     * @return string 
     */
    public function getPastores()
    {
        return $this->pastores;
    }

    /**
     * Set sedePpal
     *
     * @param string $sedePpal
     * @return sedePpal
     */
    public function setSedePpal($sedePpal)
    {
        $this->sedePpal = $sedePpal;
    
        return $this;
    }

    /**
     * Get sedePpal
     *
     * @return string 
     */
    public function getSedePpal()
    {
        return $this->sedePpal;
    }
}
