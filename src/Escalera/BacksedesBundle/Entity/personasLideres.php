<?php

namespace Escalera\BacksedesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * personasLideres
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class personasLideres
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo_mci", type="string", length=255)
     */
    private $codigoMci;

    /**
     * @var string
     *
     * @ORM\Column(name="nombres", type="string", length=255)
     */
    private $nombres;

    /**
     * @var string
     *
     * @ORM\Column(name="apellidos", type="string", length=255)
     */
    private $apellidos;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Escalera\BacksedesBundle\Entity\equipoDoce")
     */
    private $equipo12;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Escalera\BacksedesBundle\Entity\personasLideres")
     */
    private $idliderinmediato;

    /**
     * @var string
     * @Assert\NotBlank(message = "Por favor, selecciona el género")
     * @ORM\Column(name="genero", type="string", length=255)
     */
    private $genero;

    /**
     * @var boolean
     *
     * @ORM\Column(name="celula", type="boolean", nullable=false)
     */
    private $celula;

    /**
     * @var string
     * 
     * @ORM\Column(name="emaillider", type="string", length=255, nullable=false)
     * @Assert\Email()
     */
    private $emaillider;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_nacimiento", type="date")
     */
    private $fechaNacimiento;

    /**
     * @var string
     * @Assert\NotBlank(message = "Por favor, estado civil")
     * @ORM\Column(name="estado_civil", type="text", length=255, nullable=false)
     */
    private $estadoCivil;

    /**
     * @var string
     *
     * @ORM\OneToOne(targetEntity="Escalera\BacksedesBundle\Entity\personasLideres")
     */
    private $conyuge;

    /**
     * @var string
     * 
     * @ORM\Column(name="foto", type="text", length=255, nullable=true)
     */
    private $foto;

    /**
     * @var string
     * @Assert\NotBlank(message = "No dejes en blanco, coloca 0000")
     * @ORM\Column(name="telefono_casa", type="text", length=255, nullable=false)
     */
    private $telefonoCasa;

    /**
     * @var string
     * @Assert\NotBlank(message = "No dejes en blanco, coloca 0000")
     * @ORM\Column(name="telefono_oficina", type="string", length=255, nullable=false)
     */
    private $telefonoOficina;

    /**
     * @var string
     * @Assert\NotBlank(message = "No dejes en blanco, coloca 0000")
     * @ORM\Column(name="celular", type="string", length=255, nullable=false)
     */
    private $celular;

    /**
     * @var boolean
     *
     * @ORM\Column(name="estado", type="boolean")
     */
    private $estado;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=false)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="salt", type="string", length=255, nullable=false)
     */
    private $salt;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=255, nullable=false)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="ROLE", type="string", length=255, nullable=false)
     */
    private $rOLE;

    /**
     * @var string
     *
     * @ORM\Column(name="ganadoasoc", type="string", length=255, nullable=false)
     */
    private $ganadoasoc;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Escalera\BacksedesBundle\Entity\sedeDirecta")
     */
    private $sedeId;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codigoMci
     *
     * @param string $codigoMci
     * @return personasLideres
     */
    public function setCodigoMci($codigoMci)
    {
        $this->codigoMci = $codigoMci;
    
        return $this;
    }

    /**
     * Get codigoMci
     *
     * @return string 
     */
    public function getCodigoMci()
    {
        return $this->codigoMci;
    }

    /**
     * Set nombres
     *
     * @param string $nombres
     * @return personasLideres
     */
    public function setNombres($nombres)
    {
        $this->nombres = $nombres;
    
        return $this;
    }

    /**
     * Get nombres
     *
     * @return string 
     */
    public function getNombres()
    {
        return $this->nombres;
    }

    /**
     * Set apellidos
     *
     * @param string $apellidos
     * @return personasLideres
     */
    public function setApellidos($apellidos)
    {
        $this->apellidos = $apellidos;
    
        return $this;
    }

    /**
     * Get apellidos
     *
     * @return string 
     */
    public function getApellidos()
    {
        return $this->apellidos;
    }

    /**
     * Set equipo12
     *
     * @param string $equipo12
     * @return personasLideres
     */
    public function setEquipo12(\Escalera\BacksedesBundle\Entity\equipoDoce $equipo12)
    {
        $this->equipo12 = $equipo12;
    
        return $this;
    }

    /**
     * Get equipo12
     *
     * @return string 
     */
    public function getEquipo12()
    {
        return $this->equipo12;
    }

    /**
     * Set idliderinmediato
     *
     * @param string $idliderinmediato
     * @return personasLideres
     */
    public function setIdliderinmediato(\Escalera\BacksedesBundle\Entity\personasLideres $idliderinmediato)
    {
        $this->idliderinmediato = $idliderinmediato;
    
        return $this;
    }

    /**
     * Get idliderinmediato
     *
     * @return string 
     */
    public function getIdliderinmediato()
    {
        return $this->idliderinmediato;
    }

    /**
     * Set genero
     *
     * @param string $genero
     * @return personasLideres
     */
    public function setGenero($genero)
    {
        $this->genero = $genero;
    
        return $this;
    }

    /**
     * Get genero
     *
     * @return string 
     */
    public function getGenero()
    {
        return $this->genero;
    }

    /**
     * Set celula
     *
     * @param string $celula
     * @return personasLideres
     */
    public function setCelula($celula)
    {
        $this->celula = $celula;
    
        return $this;
    }

    /**
     * Get celula
     *
     * @return string 
     */
    public function getCelula()
    {
        return $this->celula;
    }

    /**
     * Set emaillider
     *
     * @param string $emaillider
     * @return personasLideres
     */
    public function setEmaillider($emaillider)
    {
        $this->emaillider = $emaillider;
    
        return $this;
    }

    /**
     * Get emaillider
     *
     * @return string 
     */
    public function getEmaillider()
    {
        return $this->emaillider;
    }

    /**
     * Set fechaNacimiento
     *
     * @param \DateTime $fechaNacimiento
     * @return personasLideres
     */
    public function setFechaNacimiento($fechaNacimiento)
    {
        $this->fechaNacimiento = $fechaNacimiento;
    
        return $this;
    }

    /**
     * Get fechaNacimiento
     *
     * @return \DateTime 
     */
    public function getFechaNacimiento()
    {
        return $this->fechaNacimiento;
    }

    /**
     * Set estadoCivil
     *
     * @param string $estadoCivil
     * @return personasLideres
     */
    public function setEstadoCivil($estadoCivil)
    {
        $this->estadoCivil = $estadoCivil;
    
        return $this;
    }

    /**
     * Get estadoCivil
     *
     * @return string 
     */
    public function getEstadoCivil()
    {
        return $this->estadoCivil;
    }

    /**
     * Set conyuge
     *
     * @param string $conyuge
     * @return personasLideres
     */
    public function setConyuge(\Escalera\BacksedesBundle\Entity\personasLideres $conyuge)
    {
        $this->conyuge = $conyuge;
    
        return $this;
    }

    /**
     * Get conyuge
     *
     * @return string 
     */
    public function getConyuge()
    {
        return $this->conyuge;
    }

    /**
     * Set foto
     *
     * @param string $foto
     * @return personasLideres
     */
    public function setFoto($foto)
    {
        $this->foto = $foto;
    
        return $this;
    }

    /**
     * Get foto
     *
     * @return string 
     */
    public function getFoto()
    {
        return $this->foto;
    }

    /**
     * Set telefonoCasa
     *
     * @param string $telefonoCasa
     * @return personasLideres
     */
    public function setTelefonoCasa($telefonoCasa)
    {
        $this->telefonoCasa = $telefonoCasa;
    
        return $this;
    }

    /**
     * Get telefonoCasa
     *
     * @return string 
     */
    public function getTelefonoCasa()
    {
        return $this->telefonoCasa;
    }

    /**
     * Set telefonoOficina
     *
     * @param string $telefonoOficina
     * @return personasLideres
     */
    public function setTelefonoOficina($telefonoOficina)
    {
        $this->telefonoOficina = $telefonoOficina;
    
        return $this;
    }

    /**
     * Get telefonoOficina
     *
     * @return string 
     */
    public function getTelefonoOficina()
    {
        return $this->telefonoOficina;
    }

    /**
     * Set celular
     *
     * @param string $celular
     * @return personasLideres
     */
    public function setCelular($celular)
    {
        $this->celular = $celular;
    
        return $this;
    }

    /**
     * Get celular
     *
     * @return string 
     */
    public function getCelular()
    {
        return $this->celular;
    }

    /**
     * Set estado
     *
     * @param boolean $estado
     * @return personasLideres
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    
        return $this;
    }

    /**
     * Get estado
     *
     * @return boolean 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return personasLideres
     */
    public function setPassword($password)
    {
        $this->password = $password;
    
        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set salt
     *
     * @param string $salt
     * @return personasLideres
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    
        return $this;
    }

    /**
     * Get salt
     *
     * @return string 
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return personasLideres
     */
    public function setUsername($username)
    {
        $this->username = $username;
    
        return $this;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set rOLE
     *
     * @param string $rOLE
     * @return personasLideres
     */
    public function setROLE($rOLE)
    {
        $this->rOLE = $rOLE;
    
        return $this;
    }

    /**
     * Get rOLE
     *
     * @return string 
     */
    public function getROLE()
    {
        return $this->rOLE;
    }

    /**
     * Set ganadoasoc
     *
     * @param string $ganadoasoc
     * @return personasLideres
     */
    public function setGanadoasoc($ganadoasoc)
    {
        $this->ganadoasoc = $ganadoasoc;
    
        return $this;
    }

    /**
     * Get ganadoasoc
     *
     * @return string 
     */
    public function getGanadoasoc()
    {
        return $this->ganadoasoc;
    }

    /**
     * Set sedeId
     *
     * @param string $sedeId
     * @return personasLideres
     */
    public function setSedeId(\Escalera\BacksedesBundle\Entity\sedeDirecta $sedeId)
    {
        $this->sedeId = $sedeId;
    
        return $this;
    }

    /**
     * Get sedeId
     *
     * @return string 
     */
    public function getSedeId()
    {
        return $this->sedeId;
    }
    public function __toString()
    {
	return $this->getNombres()." ".$this->getApellidos();
    }
}
