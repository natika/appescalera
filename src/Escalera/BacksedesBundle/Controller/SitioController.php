<?php
namespace Escalera\BacksedesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SitioController extends Controller
{
	public function estaticaAction($pagina)
	{
		return $this->render('EscaleraBacksedesBundle:Sitio:'.$pagina.'.html.twig');
	}
}
