<?php

namespace Escalera\BacksedesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function ayudaAction()
    {
	return $this->render('EscaleraBacksedesBundle:Default:ayuda.html.twig');
    }  
}
