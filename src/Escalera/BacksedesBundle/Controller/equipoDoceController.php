<?php

namespace Escalera\BacksedesBundle\Controller;

use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Escalera\BacksedesBundle\Entity\equipoDoce;
use Escalera\BacksedesBundle\Form\equipoDoceType;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;

/**
 * equipoDoce controller.
 *
 */
class equipoDoceController extends Controller
{

    /**
     * Lists all equipoDoce entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $usuarios = $this->get('security.context')->getToken()->getUser();
        $misede = $usuarios->getSede();
        $entities = $em->getRepository('EscaleraBacksedesBundle:equipoDoce')->findBy(array(
            'sedeId'=>$misede, 'activo'=> '1',
        ));

        return $this->render('EscaleraBacksedesBundle:equipoDoce:index.html.twig', array(
            'llenar'=>'No',
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new equipoDoce entity.
     *
     */
    public function createAction(Request $request)
    {
        //capturar datos usuario
       
        $em = $this->getDoctrine()->getManager();
        $usuarios = $this->get('security.context')->getToken()->getUser();
        $misede = $usuarios->getSede();
        //////////////////////////////
        $entity  = new equipoDoce();
        
        $form = $this->createForm(new equipoDoceType($misede), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('doce_show', array('id' => $entity->getId())));
        }

        return $this->render('EscaleraBacksedesBundle:equipoDoce:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new equipoDoce entity.
     *
     */
    public function newAction()
    {
         //capturar datos usuario
        
        $em = $this->getDoctrine()->getManager();
        $usuarios = $this->get('security.context')->getToken()->getUser();
        $misede = $usuarios->getSede();
        //////////////////////////////
        $entity = new equipoDoce();
        $entity->setActivo(true);
        $entity->setFechaIngreso(new \DateTime('now'));
        $form   = $this->createForm(new equipoDoceType($misede), $entity);

        return $this->render('EscaleraBacksedesBundle:equipoDoce:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a equipoDoce entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $usuarios = $this->get('security.context')->getToken()->getUser();
        $misede = $usuarios->getSede();
        $entity = $em->getRepository('EscaleraBacksedesBundle:equipoDoce')->findOneBy(array(
            'id'=>$id,
            'sedeId'=>$misede));
        
        if (!$entity) {
            throw $this->createNotFoundException('No existe');
        }

        $deleteForm = $this->createDeleteForm($id);
        return $this->render('EscaleraBacksedesBundle:equipoDoce:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing equipoDoce entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $usuarios = $this->get('security.context')->getToken()->getUser();
        $misede = $usuarios->getSede();
        $entity = $em->getRepository('EscaleraBacksedesBundle:equipoDoce')->findOneBy(array(
            'id'=>$id,
            'sedeId'=>$misede));

        if (!$entity) {
            throw $this->createNotFoundException('No existe!!!');
        }

        $editForm = $this->createForm(new equipoDoceType($misede), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('EscaleraBacksedesBundle:equipoDoce:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing equipoDoce entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        try
        {
            //Se fue la base de datos
        }
        catch (Exception $ex)
        {
            //iNTENTA DE NUEVO
        }
        $em = $this->getDoctrine()->getManager();
        $usuarios = $this->get('security.context')->getToken()->getUser();
        $misede = $usuarios->getSede();
        $entity = $em->getRepository('EscaleraBacksedesBundle:equipoDoce')->findOneBy(array(
            'id'=>$id,
            'sedeId'=>$misede));

        if (!$entity) {
            throw $this->createNotFoundException('Hmmmmmm pilas.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new equipoDoceType($misede), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('doce_edit', array('id' => $id)));
        }

        return $this->render('EscaleraBacksedesBundle:equipoDoce:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a equipoDoce entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('EscaleraBacksedesBundle:equipoDoce')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find equipoDoce entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('doce'));
    }

    /**
     * Creates a form to delete a equipoDoce entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
    public function activosdoceAction()
    {
        $em = $this->getDoctrine()->getManager();
        $usuarios = $this->get('security.context')->getToken()->getUser();
        $misede = $usuarios->getSede();
        $entities = $em->getRepository('EscaleraBacksedesBundle:equipoDoce')->findBy(array(
            'sedeId'=>$misede,
            'activo'=>true,
        ));

        return $this->render('EscaleraBacksedesBundle:equipoDoce:index.html.twig', array(
            'lider12' => null,
            'llenar'=>'llenar',
            'entities' => $entities,
        ));
    }
    /*
     * Consulta de las células que pertenecen a un líder de doce
     */
    public function celulasdoceAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $usuarios = $this->get('security.context')->getToken()->getUser();
        $misede = $usuarios->getSede();
        $consulta = $em->getRepository('EscaleraBacksedesBundle:equipoDoce')->findCelulasEquipo($id,$misede);
        $paginator = $this->get('knp_paginator');
        $lideres = $paginator->paginate($consulta,$this->getRequest()->query->get('page',1)/*numero pagina */,
            10 /* Limite por paginas */
        );

        //if (!$lideres) {
          //  throw $this->createNotFoundException('Hmmmmmm, Huston, tenemos problemas');
        //}
        
        return $this->render('EscaleraBacksedesBundle:infocelula:index.html.twig', array(
            'lider12' => $id,
            'entities' => $lideres,
            'tipo'=>'semana',
        ));
    }
}
