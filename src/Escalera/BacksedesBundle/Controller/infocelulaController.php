<?php

namespace Escalera\BacksedesBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Escalera\BacksedesBundle\Entity\infocelula;
use Escalera\BacksedesBundle\Form\infocelulaType;
use Escalera\BacksedesBundle\Entity\celulasinfo;
use Escalera\BacksedesBundle\Form\celulasinfoType;

/**
 * infocelula controller.
 *
 */
class infocelulaController extends Controller
{
     
    /**
     * Lists all infocelula entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        //capturar datos usuario
        $usuarios = $this->get('security.context')->getToken()->getUser();
        $misede = $usuarios->getSede();
        //////////////////////////////
        $consulta = $em->getRepository('EscaleraBacksedesBundle:infocelula')->findBy(array(
            'sede'=>$misede
        ));
        $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate($consulta,$this->getRequest()->query->get('page',1)/*numero pagina */,
            10 /* Limite por paginas */    
        );
        return $this->render('EscaleraBacksedesBundle:infocelula:index.html.twig', array(
            'entities' => $entities,
            'tipo'=>'celula',
        ));
    }
    /**
     * Creates a new infocelula entity.
     *
     */
    public function createAction(Request $request)
    {
         //capturar datos usuario
        $em = $this->getDoctrine()->getManager();
        $usuarios = $this->get('security.context')->getToken()->getUser();
        $misede = $usuarios->getSede();
        //////////////////////////////
        $entity  = new infocelula();
        $form = $this->createForm(new infocelulaType($misede), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('celulas_show', array('id' => $entity->getId())));
        }

        return $this->render('EscaleraBacksedesBundle:infocelula:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new infocelula entity.
     *
     */
    public function newAction()
    {
        //capturar datos usuario
        $usuarios = $this->get('security.context')->getToken()->getUser();
        $misede = $usuarios->getSede();
        //////////////////////////////
        $entity = new infocelula();
        $entity->setSede($misede);
        $entity->setEstado(true);
        $form   = $this->createForm(new infocelulaType($misede), $entity);

        return $this->render('EscaleraBacksedesBundle:infocelula:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a infocelula entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        //capturar datos usuario
        $usuarios = $this->get('security.context')->getToken()->getUser();
        $misede = $usuarios->getSede();
        //////////////////////////////
        $celus = new celulasinfo();
        $celus->setNovedad('...');
        $celus->setFechaRealizacion(new \Datetime('now'));
        $form   = $this->createForm(new celulasinfoType($id), $celus);
        $entity = $em->getRepository('EscaleraBacksedesBundle:infocelula')->findOneBy(array(
          'id'=>$id,
          'sede'=>$misede,
        ));
        //// obtener lider de doce para poder regresar al listado de sus células /////////////////
        /////////// Obtenemos el líder y con ese lider obtenemos el equipo /////////
        $lider = $entity->getIdlidercelula();
        $equipo = $em->getRepository('EscaleraBacksedesBundle:personasLideres')->findOneById($lider);
        ////////// pero al obtener el equipo tengo un objeto de Equipo completo,
        //////////// solo necesito el id del equipo para ponerlo en un link de regreso :D
        $equipos = $equipo->getEquipo12();
        //////////// finalmente puedo capturar el Id del objeto obtenido mediante
        /////////// $equipo->getEquipo12() y lo paso a $equipo final
        $equipofinal = $em->getRepository('EscaleraBacksedesBundle:equipoDoce')->findOneById($equipos);
        $equipofinal->getId();
        ///// filtrar células relizadas //////////////
        $realizadas = $em->getRepository('EscaleraBacksedesBundle:celulasinfo')->findRealizadas($id);
        
        if (!$entity) {
            throw $this->createNotFoundException('No existe esa célula.');
        }
        ////// Insertar registros de la célula ////////// 
        $peticion = $this->getRequest();
        $entity2  = new celulasinfo();
        $form2 = $this->createForm(new celulasinfoType($id), $entity2);
        $form2->bind($peticion);

        if ($form2->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity2);
            $em->flush();

            return $this->redirect($this->generateUrl('celulas_show', array('id' => $entity->getId())));
        }
        /////// Fin de insertar registros //////////////
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('EscaleraBacksedesBundle:infocelula:show.html.twig', array(
            'realizadas'   => $realizadas, /// celulas realizadas
            'equipofinal' => $equipofinal, /// Id del equipo
            'equipo'      => $equipo, ///// Datos del Equipo
            'entity'      => $entity, ///// Datos de la célula
            'delete_form' => $deleteForm->createView(), 
            'form'        =>$form->createView(),
            ));
    }

    /**
     * Displays a form to edit an existing infocelula entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $usuarios = $this->get('security.context')->getToken()->getUser();
        $misede = $usuarios->getSede();
        $entity = $em->getRepository('EscaleraBacksedesBundle:infocelula')->findOneBy(array(
            'id'=>$id,
            'sede'=>$misede,
            ));

        if (!$entity) {
            throw $this->createNotFoundException('Pilas que no existe.');
        }

        $editForm = $this->createForm(new infocelulaType($misede), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('EscaleraBacksedesBundle:infocelula:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing infocelula entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $usuarios = $this->get('security.context')->getToken()->getUser();
        $misede = $usuarios->getSede();
        $entity = $em->getRepository('EscaleraBacksedesBundle:infocelula')->findOneBy(array(
            'id'=>$id,
            'sede'=>$misede,
            ));

        if (!$entity) {
            throw $tphis->createNotFoundException('Unable to find infocelula entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new infocelulaType($misede), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('celulas_edit', array('id' => $id)));
        }

        return $this->render('EscaleraBacksedesBundle:infocelula:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a infocelula entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('EscaleraBacksedesBundle:infocelula')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find infocelula entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('celulas'));
    }

    /**
     * Creates a form to delete a infocelula entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
