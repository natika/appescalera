<?php

namespace Escalera\BacksedesBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Escalera\BacksedesBundle\Entity\red;
use Escalera\BacksedesBundle\Form\redType;

/**
 * red controller.
 *
 */
class redController extends Controller
{

    /**
     * Lists all red entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('EscaleraBacksedesBundle:red')->findAll();

        return $this->render('EscaleraBacksedesBundle:red:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new red entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new red();
        $form = $this->createForm(new redType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('red_show', array('id' => $entity->getId())));
        }

        return $this->render('EscaleraBacksedesBundle:red:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new red entity.
     *
     */
    public function newAction()
    {
        $entity = new red();
        $form   = $this->createForm(new redType(), $entity);

        return $this->render('EscaleraBacksedesBundle:red:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a red entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('EscaleraBacksedesBundle:red')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find red entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('EscaleraBacksedesBundle:red:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing red entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('EscaleraBacksedesBundle:red')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find red entity.');
        }

        $editForm = $this->createForm(new redType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('EscaleraBacksedesBundle:red:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing red entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('EscaleraBacksedesBundle:red')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find red entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new redType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('red_edit', array('id' => $id)));
        }

        return $this->render('EscaleraBacksedesBundle:red:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a red entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('EscaleraBacksedesBundle:red')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find red entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('red'));
    }

    /**
     * Creates a form to delete a red entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
