<?php

namespace Escalera\BacksedesBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Escalera\BacksedesBundle\Entity\celulasinfo;
use Escalera\BacksedesBundle\Form\celulasinfoType;

/**
 * celulasinfo controller.
 *
 */
class celulasinfoController extends Controller
{

    /**
     * Lists all celulasinfo entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('EscaleraBacksedesBundle:celulasinfo')->findAll();

        return $this->render('EscaleraBacksedesBundle:celulasinfo:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new celulasinfo entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new celulasinfo();
        $form = $this->createForm(new celulasinfoType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('info_show', array('id' => $entity->getId())));
        }

        return $this->render('EscaleraBacksedesBundle:celulasinfo:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new celulasinfo entity.
     *
     */
    public function newAction()
    {
        $entity = new celulasinfo();
        $form   = $this->createForm(new celulasinfoType(), $entity);

        return $this->render('EscaleraBacksedesBundle:celulasinfo:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a celulasinfo entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('EscaleraBacksedesBundle:celulasinfo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find celulasinfo entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('EscaleraBacksedesBundle:celulasinfo:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing celulasinfo entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('EscaleraBacksedesBundle:celulasinfo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find celulasinfo entity.');
        }

        $editForm = $this->createForm(new celulasinfoType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('EscaleraBacksedesBundle:celulasinfo:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing celulasinfo entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('EscaleraBacksedesBundle:celulasinfo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find celulasinfo entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new celulasinfoType(), $entity);
        $editForm->bind($request);
        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('info_edit', array('id' => $id)));
        }

        return $this->render('EscaleraBacksedesBundle:celulasinfo:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a celulasinfo entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('EscaleraBacksedesBundle:celulasinfo')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find celulasinfo entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('info'));
    }

    /**
     * Creates a form to delete a celulasinfo entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
