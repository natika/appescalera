<?php

namespace Escalera\BacksedesBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Escalera\BacksedesBundle\Form\UsuariosType;
use Escalera\BacksedesBundle\Form\reunionesType;
use Escalera\BacksedesBundle\Form\personasLideresType;
use Escalera\BacksedesBundle\Entity\reuniones;
use Escalera\BacksedesBundle\Entity\sedeDirecta;
use Escalera\BacksedesBundle\Entity\personasLideres;


class BacksedesController extends Controller
{
    public function portadaAction()
    {
        return $this->render('EscaleraBacksedesBundle:Default:index.html.twig');
    }
    public function ayudaAction()
    {
	return $this->render('EscaleraBacksedesBundle:Default:ayuda.html.twig');
    }  
    public function loginAction()
    {
        $peticion = $this->getRequest();
        $sesion = $peticion->getSession();
        $error = $peticion->attributes->get(
             SecurityContext::AUTHENTICATION_ERROR,
             $sesion->get(SecurityContext::AUTHENTICATION_ERROR)
             );
        return $this->render('EscaleraBacksedesBundle:Backsedes:login.html.twig', array(
            'error' => $error
        ));
    }
    public function perfilAction()
    {
        
        //obtener datos del usuario logueado y utizarlos para llenar el formu de registro
        $usuarios = $this->get('security.context')->getToken()->getUser();
        $formulario = $this->createForm(new UsuariosType(), $usuarios);
        //si la petición es GET, mostrar formulario
        //si la petición es POST, actualizar info del usuario con nuevos datos del formulario
        $peticion = $this->getRequest();
        if($peticion->getMethod() == 'POST'){
            $passwordOriginal = $formulario->getData()->getPassword(); //atrapar el pass
            $formulario->bind($peticion);//Recuerda que en los ejemplos usan bindRequest
            
            if($formulario->isValid()){
                if(null == $usuarios->getPassword()){
                    $usuarios->setPassword($passwordOriginal);
                }
                else{
                    $encoder = $this->get('security.encoder_factory')
                                    ->getEncoder($usuarios);
                    $passwordCodificado = $encoder->encodePassword(
                            $usuarios -> getPassword(),
                            $usuarios -> getSalt()
                    );
                    $usuarios->setPassword($passwordCodificado);
                }
                $errores = $this->get('validator')->validate($formulario);
                //actualizamos el perfil del usuario
                $em = $this->getDoctrine()->getManager();
                $em->persist($usuarios);
                $em->flush();
                
                //$this->getUser()->setFlash('notice','Actualizado');
                return $this->redirect($this->generateUrl('backsedes_perfil'));
            }
        }
        //si no es post entonces es GET, le pintamos un formu
        return $this->render('EscaleraBacksedesBundle:Backsedes:perfil.html.twig',array(
            'usuarios'   => $usuarios,
            'formulario' => $formulario->createView()
        ));
    }
    public function reunionAction()
    {
        $reuniones = new reuniones();
        $reuniones->setActivo(true);
        $usuarios = $this->get('security.context')->getToken()->getUser();
        $entidad = $usuarios->getSede();
        $reuniones->setSedeId($entidad);
        $peticion = $this->getRequest();
        $formulario = $this->createForm(new reunionesType, $reuniones); 
        //$formulario = $this->
     //capturamos la Petición si es GET o POST
        //$formulario->handleRequest($request);
         if($peticion->getMethod() == 'POST'){ 
              $formulario->bind($peticion);
              if($formulario->isValid()){
                // persistir en la BD
                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($reuniones);
                $em->flush();
                return $this->redirect($this->generateUrl('backsedes_reuniones'));
               // $ok = 'Bien';
              }
         }
 
         $em = $this->getDoctrine()->getManager();
       // $todasReuniones = $em->getReference('EscaleraBacksedesBundle:reuniones')->findTodasLasReuniones($entidad);
        $reunionestotal = $em->getRepository('EscaleraBacksedesBundle:Reuniones')->findBySedeId($entidad); 
         return $this->render('EscaleraBacksedesBundle:Backsedes:reunion.html.twig', array(
            'formulario' => $formulario->createView(),
             'reunionestotal' => $reunionestotal,
         ));
        
    }
    public function editarReunionAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $reunion = $em->getRepository('EscaleraBacksedesBundle:reuniones')->find($id);
        
        if(!$reunion){
            throw $this->createNotFoundException('No existe esa reunión');
        }
        $peticion = $this->getRequest();
        $formulario = $this->createForm(new reunionesType, $reunion);
        
        
         if($peticion->getMethod() == 'POST'){ 
                $formulario->bind($peticion);
                
              if($formulario->isValid()){
                // persistir en la BD
                $em = $this->getDoctrine()->getManager();
                $em->persist($reunion);
                $em->flush();
                return $this->redirect($this->generateUrl('backsedes_reuniones'));
               // $ok = 'Bien';
              }
         }
        
        return $this->render('EscaleraBacksedesBundle:Backsedes:editReunion.html.twig', array(
            'reunion' => $reunion,
            'formulario' => $formulario->createView(),
        ));
        
        
    }
    public function lideresAction()
    {
        $usuarios = $this->get('security.context')->getToken()->getUser();
        $misede = $usuarios->getSede();
        $lider = new personasLideres();
        $lider->setSedeId($misede);
        $lider->setCodigoMci('0302');
        $lider->setEstado(true);
        $lider->setCelula(false);
        $lider->setFoto('false');
        $lider->setTelefonoCasa(0);
        $lider->setTelefonoOficina(0);
        $lider->setCelular(0);
        $lider->setUsername('none');
        $lider->setROLE('ROLE_LIDER');
        $lider->setPassword(123456);
        $lider->setSalt('nada');
        $lider->setGanadoasoc('null');
        $formulario = $this->createForm(new personasLideresType($misede), $lider);
        $peticion = $this->getRequest();
        if($peticion->getMethod() == 'POST'){
           $formulario->bind($peticion);
            if($formulario->isValid()){
                $em = $this->getDoctrine()->getManager();
                $em->persist($lider);
                $em->flush();
                return $this->redirect($this->generateUrl('backsedes_lideres'));
            }
        }
        
        return $this->render('EscaleraBacksedesBundle:Backsedes:lideres.html.twig', array(
            'accion' => 'crear',
            'formulario' => $formulario->createView(),
        ));
    }
    public function lideresshowAction()
    {
        $em= $this->getDoctrine()->getManager();
        $usuario = $this->get('security.context')->getToken()->getUser();
        $misede = $usuario->getSede();
        $paginator = $this->get('knp_paginator');
        $consulta = $em->getRepository('EscaleraBacksedesBundle:personasLideres')->findBy(array('sedeId'=> $misede));
        $lideres = $paginator->paginate($consulta,$this->getRequest()->query->get('page',1)/*numero pagina */,
            10 /* Limite por paginas */    
        );
        return $this->render('EscaleraBacksedesBundle:Backsedes:lideresedit.html.twig', array(
            'lideres'=>$lideres,
        ));
    }
    public function lideresEditarAction($id)
    {
        
        $em = $this->getDoctrine()->getManager();
        $usuarios = $this->get('security.context')->getToken()->getUser();
        $misede= $usuarios->getSede();
        
        $lider = $em->getRepository('EscaleraBacksedesBundle:personasLideres')->findOneBy(
                array('id'=>$id, 'sedeId'=>$misede)
                ); 
        if(!$lider){
            throw $this->createNotFoundException('No existe ese líder');
        }
        $peticion = $this->getRequest();
        $formulario = $this->createForm(new personasLideresType($misede), $lider);
        
        if($peticion->getMethod() == 'POST'){ 
                $formulario->bind($peticion);
                
              if($formulario->isValid()){
                // persistir en la BD
                $em = $this->getDoctrine()->getManager();
                $em->persist($lider);
                $em->flush();
                return $this->redirect($this->generateUrl('backsedes_lideres_show'));
               // $ok = 'Bien';
              }
         }
        
        return $this->render('EscaleraBacksedesBundle:Backsedes:lideres.html.twig', array(
            'accion' => 'editar',
            'sede' => $misede,
            'lider' => $lider,
            'formulario' => $formulario->createView(),
        ));
        
        
    }
}
