<?php

namespace Escalera\BacksedesBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Escalera\BacksedesBundle\Entity\ganados;
use Escalera\BacksedesBundle\Form\ganadosType;

/**
 * ganados controller.
 *
 */
class ganadosController extends Controller
{

    /**
     * Lists all ganados entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $consulta = $em->getRepository('EscaleraBacksedesBundle:ganados')->findAll();
        //ANEXAMOS PAGINADOR
        $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate($consulta,$this->getRequest()->query->get('page',1)/*numero pagina */,
            10 /* Limite por paginas */    
        );
        return $this->render('EscaleraBacksedesBundle:ganados:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new ganados entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new ganados();
        $form = $this->createForm(new ganadosType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('ganados_show', array('id' => $entity->getId())));
        }

        return $this->render('EscaleraBacksedesBundle:ganados:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new ganados entity.
     *
     */
    public function newAction()
    {
        $entity = new ganados();
        $entity->setAscLider(false);
        $entity->setVisita(false);
        $entity->setFonovisita(false);
        $entity->setAsistecelula(false);
        $form   = $this->createForm(new ganadosType(), $entity);

        return $this->render('EscaleraBacksedesBundle:ganados:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a ganados entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('EscaleraBacksedesBundle:ganados')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ganados entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('EscaleraBacksedesBundle:ganados:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing ganados entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('EscaleraBacksedesBundle:ganados')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ganados entity.');
        }

        $editForm = $this->createForm(new ganadosType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('EscaleraBacksedesBundle:ganados:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing ganados entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('EscaleraBacksedesBundle:ganados')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ganados entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new ganadosType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('ganados_edit', array('id' => $id)));
        }

        return $this->render('EscaleraBacksedesBundle:ganados:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a ganados entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('EscaleraBacksedesBundle:ganados')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find ganados entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('ganados'));
    }

    /**
     * Creates a form to delete a ganados entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
