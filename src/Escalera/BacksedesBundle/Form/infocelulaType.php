<?php

namespace Escalera\BacksedesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class infocelulaType extends AbstractType
{
    public function __construct($sede)
    {
        $this->sede = $sede;
    } 
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
	$self = $this;
        $builder
            ->add('codigocelula')
            ->add('fechaapertura',null,array(
                'attr'=>array('label'=>'Fecha de Apertura')
            ))
            ->add('hora','time',array(
                'input'=>'datetime',
                'widget'=>'choice',
            ))
            ->add('novedad')
            ->add('anfitrion')
            ->add('direccion')
            ->add('barrio')
            ->add('estado')
            ->add('idlidercelula','entity',array(
                'class'=>'EscaleraBacksedesBundle:personasLideres',
                'query_builder'=>function(EntityRepository $er) use($self){
                        return $er->createQueryBuilder('u')
                                ->where('u.estado = true and u.sedeId = :sede')
                                ->setParameter('sede',$self->sede)
                                ->orderBy('u.nombres','ASC');
                },
                'label'=>'Lider de la célula',
                'empty_value'=>'Escoge un líder',
                'required'=>true,
                
            ))
            ->add('sede','entity',array(
                'class'=>'EscaleraBacksedesBundle:sedeDirecta',
                'query_builder'=>  function(EntityRepository $er) use($self){
                        return $er->createQueryBuilder('u')
                                ->where('u.id = :sede')
                                ->setParameter('sede', $self->sede);
                },
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Escalera\BacksedesBundle\Entity\infocelula'
        ));
    }

    public function getName()
    {
        return 'escalera_backsedesbundle_infocelulatype';
    }
}
