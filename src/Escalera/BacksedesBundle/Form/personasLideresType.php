<?php

namespace Escalera\BacksedesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class personasLideresType extends AbstractType
{
    public function __construct($sede)
    {
        $this->sede = $sede;
    }    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
	$self = $this;
        $builder
            ->add('codigoMci')
            ->add('nombres')
            ->add('apellidos',null,array(
                'attr'=>array('placeholder'=>'Apellidos acá'),
            ))
            ->add('idliderinmediato','entity',array(
                'class'=>'EscaleraBacksedesBundle:personasLideres',
                'query_builder'=>function(EntityRepository $er) use($self) {
                        return $er->createQueryBuilder('u')
                                ->where('u.estado = true and u.sedeId = :sede')
                                ->setParameter('sede',$self->sede)
                                ->orderBy('u.nombres','ASC');
                },
                'required'=>false,
            ))
            ->add('genero','choice',array(
                'choices'=>array('F'=>'Femenino','M'=>'Masculino'),
                'empty_value' => 'Selecciona tu género',
                'required'=>true,
            ))
            ->add('celula',null,array(
                'attr'=>array('class'=>'iphone-toggle'),
                'required'=>false))
            ->add('emaillider','email')
            ->add('fechaNacimiento','birthday',array('required'=>false))
            ->add('estadoCivil','choice',array(
                'empty_value'=>false,
                'choices'=> array('desconocido'=>'desconocido','S'=>'Soltero(a)','C'=>'Casado(a)','U'=>'Unión Libre','V'=>'Viudo(a)','D'=>'Divorciado(a)'),
                'required'=>false))
            ->add('conyuge','entity',array(
                'class'=>'EscaleraBacksedesBundle:personasLideres',
                'query_builder'=>function(EntityRepository $et) use($self){
                        return $et->createQueryBuilder('a')
                                ->where('a.sedeId = :sede')
                                ->setParameter('sede',$self->sede)
                                ->orderBy('a.nombres','ASC');
                },
                 'empty_value'=>'No necesario',       
                 'required'=>false,       
                 ))
             ->add('foto',null,array(
                'attr'=>array('class'=>''),
                'required'=>false))
            ->add('telefonoCasa','text',array('required'=>false))
            ->add('telefonoOficina','text',array(
                'required'=>false
            ))
            ->add('celular','text',array('required'=>false))
            ->add('estado',null,array('required'=>false))
            ->add('password','hidden',array('required'=>false))
            ->add('salt','hidden',array('required'=>false))
            ->add('username','hidden',array('required'=>false))
            ->add('rOLE','hidden',array('required'=>false))
            ->add('ganadoasoc','hidden',array('required'=>false))
            ->add('equipo12','entity',array(
                'class'=>'EscaleraBacksedesBundle:equipoDoce',
                'query_builder'=>function(EntityRepository $ei) use($self){
                        return $ei->createQueryBuilder('a')
                                ->where('a.sedeId = :sede')
                                ->setParameter('sede',$self->sede);
                
                },
                 'required'=>false,       
                        ))
            ->add('sedeId')
            ->add('Crear','submit',array(
               'attr'=> array('class'=>'btn btn-primary dropdown-toggle')
             ))    
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Escalera\BacksedesBundle\Entity\personasLideres'
        ));
    }

    public function getName()
    {
        return 'escalera_backsedesbundle_personasLiderestype';
    }
}
