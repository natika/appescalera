<?php

namespace Escalera\BacksedesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class reunionesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('idreunion')
            ->add('detalles','textarea')
            ->add('fechaApertura','date')
                //,array(
                //'widget'=>'single_text',             
            //))
            ->add('activo',null, array('required'=>false))
            ->add('sedeId',null,array('read_only'=>true))
            ->add('Crear', 'submit', array(
               'attr'=> array('class'=>'btn btn-primary dropdown-toggle'),
             ))
            ->getForm();
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Escalera\BacksedesBundle\Entity\reuniones'
        ));
    }

    public function getName()
    {
        return 'escalera_backsedesbundle_reunionestype';
    }
}
