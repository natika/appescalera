<?php

namespace Escalera\BacksedesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class celulasinfoType extends AbstractType
{
    
    public function __construct($celula)
    {
        $this->celula = $celula;
    } 
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $self = $this;
        $builder
            ->add('fechaRealizacion')
            ->add('asistentes')
            ->add('ofrenda')
            ->add('tema')
            ->add('novedad','text',array(
                'attr'=>array('placeholder'=>'Alguna novedad'),
                'required'=>false,
            ))
            ->add('idcelula','entity',array(
                'class'=>'EscaleraBacksedesBundle:infocelula',
                'query_builder'=>function(EntityRepository $er) use($self){
                        return $er->createQueryBuilder('u')
                                ->where('u.id = :celula')
                                ->setParameter('celula',$self->celula);
                },
            ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Escalera\BacksedesBundle\Entity\celulasinfo'
        ));
    }

    public function getName()
    {
        return 'escalera_backsedesbundle_celulasinfotype';
    }
}