<?php

namespace Escalera\BacksedesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ganadosType extends AbstractType
{
    public function __construct($sede)
    {
        $this->sede = $sede;
    } 
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombres')
            ->add('apellidos')
            ->add('edad')
            ->add('telefono')
            ->add('email','email')
            ->add('direccion')
            ->add('barrio')
            ->add('fechaInvita')
            ->add('tiempoAsistencia')
            ->add('peticiones')
            ->add('genero','choice',array(
                'choices'=>array('F'=>'Femenino','M'=>'Masculino'),
                'empty_value' => 'Género',
                'required'=>true,
                ))
            ->add('seguimiento','hidden')
            ->add('fonovisita','hidden')
            ->add('fechaFonovista','hidden')
            ->add('visita','hidden')
            ->add('fechaVisita','hidden')
            ->add('asistecelula','hidden')
            ->add('ascLider','hidden')
            ->add('lider12')
            ->add('invito')
            ->add('liderasignado','hidden')
            ->add('reunion')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Escalera\BacksedesBundle\Entity\ganados'
        ));
    }

    public function getName()
    {
        return 'escalera_backsedesbundle_ganadostype';
    }
}
