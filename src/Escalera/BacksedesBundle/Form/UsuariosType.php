<?php
namespace Escalera\BacksedesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class UsuariosType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
           ->add('nombres')
           ->add('apellidos')
           ->add('email','email')
           ->add('password','repeated',array(
               'type' => 'password',
               'invalid_message' => 'Las contraseñas deben coincidir',
               'options' => array('label' => 'Contraseña'),
                 ))
           ->add('ROLE','text',array('read_only'=>false)) 
           ->add('activo')
           ->add('descripcion') 
           ->add('sede',null, array('read_only' => true)) 
           ->add('Actualizar', 'submit', array(
               'attr'=> array('class'=>'btn btn-primary dropdown-toggle'),
           ))
           ->getForm();
        ////////,array(
               //'type' => 'password',
               //'invalid_message' => 'Las contraseñas deben coincidir',
              // 'options'=>array('label' => 'Contraseña')
//           ))
        ;
    }
    public function getName() 
    {
        return 'escalera_backsedesbundle_usuariostype';
    }
}