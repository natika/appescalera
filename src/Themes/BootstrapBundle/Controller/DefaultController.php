<?php

namespace Themes\BootstrapBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Escalera\BacksedesBundle\Entity\Usuarios;
use Escalera\BacksedesBundle\Form\UsuariosType;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class DefaultController extends Controller
{
    /*
     * @Route("/")
     * @Template()
     */
    public function indexAction()
    {
        //return $this->render('ThemesBootstrapBundle:Default:index.html.twig', array('name' => $name));
        return array();        
    }
    public function registroAction()
    {
        $peticion = $this->getRequest();
        $usuarios = new Usuarios();
        
        $formulario = $this->createForm(new UsuariosType(), $usuarios);
        
        if($peticion -> getMethod() == 'POST'){
            //Validar los datos y enviarlos
            $formulario ->bind($peticion);
            
            if($formulario->isValid()){
                //guardar en la base de datos
                $encoder = $this->get('security.encoder_factory')
                                ->getEncoder($usuarios);
                $usuarios->setSalt(md5(time()));
               $passwordCodificado = $encoder->encodePassword(
                       $usuarios->getPassword(),
                       $usuarios->getSalt()
               );
               $usuarios->setPassword($passwordCodificado);
               
               $em = $this->getDoctrine()->getEntityManager();
               $em->persist($usuarios);
               $em->flush();
               
               $this->get('session')->getFlashBag('info','Nos quedó al pelo');
               $token = new UsernamePasswordToken(
                   $usuarios,
                   $usuarios->getPassword(),
                   'usuarios',
                   $usuarios->getRoles()
               );
               $this->container->get('security.context')->setToken($token);
               return $this->redirect($this->generateUrl('backsedes_perfil'));
            };
        }    
        
        
        return $this->render('ThemesBootstrapBundle:Default:registro.html.twig',
                array('formulario'=> $formulario->createView())
                );
    }
}
