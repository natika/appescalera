Escalera
========
* Versión Demo *
========
DOCUMENTACIÓN
FUNCIONAMIENTO DETALLADO “EL ARCA 2.0” CON SYMFONY 2
FRONTENED o parte frontal acceso al sistema 
LÍDERES solo pueden acceder los líderes de doce, donde pueden ver el rendimiento, células, ganados y generar informes
BACKEND ”parte de administracion” solo pueden los administradoes modificar cualquier información back-sedes, permiso –“sedes”

FRONTENED 
WEB, administrada con joomla 3.0 
Plantilla elástica 
Debe tener un formulariopara acceso de lideres 
Registro independiente del sistema interno

LIDERES 
Es necesario proporcionar una contraseña de tipo lider12 aparece la sede a que pertenece 
Las paginas que la componen son: 
Perfil: editar información personal, exepto código líder de 12
Inicio: vuelve al menú principal que contiene: 
ultimas noticias modulo de twitter
ovejas sin pastor
calendario google +
notificaciones de nuevos ganados
ganar : mis ganados: llamada a la base de datos que contiene el listado de ganados total basado en el líder logueado, contienen un link para cada persona que permite llevar “seguimiento”
asignación del líder
fonovisita
visita
asiste a celula
observación
preinscribir a encuentro 
sembrar
consolidar: 
informe  asistencia a encuentro 
proyección en proyecto 

discipular 
preinscribir a escuela de lideres / capacitación destino

enviar
informe semanal:  (mis celulas) un listado de células activas e inactivas para ver el grafico total o por fechas (ultima mes, ultima trimestre, ultimo semestre y ultimo año) o entre dos fechas 
inserción de datos semanales, es clave que una celula no puede tener 2 celulas realizadas en la misma semana y que además la fecha no puede corresponder a otra semana que no sea la semana actual.
Botón descargar informe de células (PDF, Word o excel)

Rendimiento: un checkbox con los lideres activos y con celula para generar un reporte de sus células en PDF o bien de todo el ministerio.
 Mis lideres: listado de líderes jerárquico (12,144…)
Link para editarlos

-logout

BACKEND 
Solo pueden acceder usuarios con acceso de tipo “administrador”/super admin/
Los lideres no se pueden registrar
Se puede modificar todo 
Crear lideres 
Crear células 
Crear encuentros 
Crear sedes 
Crear ganados 
Generar informes de todo tipo – ganados, encuentro, células
Puede escoger la sede activa


BACK-SEDES 
Usuarios de tipo “admin/sede”
Crear lideres 
Crear células 
Crear encuentros
Crear ganados
Generar informes de todo tipo – ganados, encuentro, células
Aparecen su sede 

